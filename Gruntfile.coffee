module.exports = (grunt)->
  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json")
    mocha:
      all:
        src:["index.html"],
        options:
          mocha:
            ignoreLeaks:true
          run:true
    watch:
      files:"test/**/*.js"
      tasks:["mocha:all"]
  })

  grunt.loadNpmTasks("grunt-contrib-jshint")
  grunt.loadNpmTasks("grunt-contrib-uglify")
  grunt.loadNpmTasks("grunt-contrib-watch")
  grunt.loadNpmTasks("grunt-mocha")
  grunt.registerTask("default",["mocha:all"])
  return