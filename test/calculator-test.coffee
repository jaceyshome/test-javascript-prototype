describe "Calculator", ->
  calculator = null
  before ()->
    console.log "before"
    calculator = new Calculator()
    return

  beforeEach ()->
    console.log "before each"
    return

  describe "Add", ->
    it "should add up two numbers correctly", ->
      expect(calculator.add(1,1)).to.be.equal(2)
      return
    it "should return NaN if passed 0 arguments"

    it "should expose a global variable by accident", ->
      sum = calculator.add(1,1)
      expect(sum).to.be(2)
      return

    it "should be really really slow", ->
      loops = 100000000
      @timeout(3500)
      for i in [0..loops-1] by 1
        i = i
      expect(i).to.be.equal loops
      return
    return

  describe "Subtract", ->
    it "should subtract two numbers correctly", ->
      expect(calculator.subtract(2,1)).to.be.equal(1)
      return
    return

  describe "Divide", ->
    it "should devide two numbers correctly", ->
      expect(calculator.divide(4,2)).to.be.equal(2)
      return
    return

  afterEach ->
    console.log "after each"
    return

  after ->
    console.log "after"
    return

  return