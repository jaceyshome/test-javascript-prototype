// Generated by CoffeeScript 1.6.3
(function() {
  describe("Hero", function() {
    var hero;
    hero = null;
    before(function() {
      hero = new Hero();
    });
    beforeEach(function() {
      console.log("hero before each");
      hero.reset();
    });
    describe.only("hero get 10 damage", function() {
      it("before: should return hero life value 100", function() {
        expect(hero.getLife()).to.be.equal(100);
      });
      it("after: should return hero life 90", function() {
        hero.reset();
        hero.getDamage(10);
        expect(hero.getLife()).to.be.equal(90);
      });
    });
    describe("hero gets 10 experience", function() {
      it("before: experience is 0", function() {
        expect(hero.getExperience()).to.be.equal(0);
      });
      it("after: experience is 10", function() {
        hero.reset();
        hero.setExperience(10);
        expect(hero.getExperience()).to.be.equal(10);
      });
    });
    describe("hero die", function() {
      it("before: should return hero life 100", function() {
        expect(hero.getLife()).to.be.equal(100);
      });
      it("after: should return hero life 0", function() {
        hero.die();
        expect(hero.getLife()).to.be.equal(0);
      });
    });
    afterEach(function() {
      console.log("hero after each");
      hero.reset();
    });
    after(function() {
      hero = null;
    });
  });

}).call(this);

/*
//@ sourceMappingURL=hero-test.map
*/
