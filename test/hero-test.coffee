describe "Hero",->
  hero = null
  before ->
    hero = new Hero()
    return

  beforeEach ->
    console.log "hero before each"
    hero.reset()
    return

  describe.only "hero get 10 damage", ->
    it "before: should return hero life value 100", ->
      expect(hero.getLife()).to.be.equal(100)
      return
    it "after: should return hero life 90", ->
      hero.reset()
      hero.getDamage(10)
      expect(hero.getLife()).to.be.equal(90)
      return
    return

  describe "hero gets 10 experience", ->
    it "before: experience is 0", ->
      expect(hero.getExperience()).to.be.equal(0)
      return
    it "after: experience is 10", ->
      hero.reset()
      hero.setExperience(10)
      expect(hero.getExperience()).to.be.equal(10)
      return
    return

  describe "hero die", ->
    it "before: should return hero life 100", ->
      expect(hero.getLife()).to.be.equal(100)
      return
    it "after: should return hero life 0", ->
      hero.die()
      expect(hero.getLife()).to.be.equal(0)
      return
    return

  afterEach ->
    console.log "hero after each"
    hero.reset()
    return

  after ->
    hero = null
    return

  return
