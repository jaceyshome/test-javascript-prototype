describe "Ping", ->
  it "should update in 500ms", (done)->
    poll.ping (message)->
      expect(message).to.be("Pong!")
      done()
      return
    return
  return

describe "View", ->
  describe "Show", ->
    it "should show in 250ms", (done)->
      view.show().then(->
        done()
      )
      return
    return
  return

describe "getTemplate", ->
  it "should get template via Ajax", (done)->
    getTemplate("list.html").then((t)->
      expect(t).to.be.ok()
      done()
    )
    return
  return

describe "Refresh", ->
  it "should update in 3000 ms", (done)->
    @timeout(3500)
    deferredRefresh().then((message)->
      expect(message).to.be("Ping!")
      done()
    )
    return
  return
