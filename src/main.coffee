@poll =
  ping: (pong)->
    setTimeout ->
      pong("Pong!")
    , 500
    return

@view =
  $el:$("#main")
  show: ->
    @$el.fadeIn(250).promise()

@getTemplate = (path, callback)->
  $.ajax({url:path})

@deferredRefresh = ->
  $.Deferred((dfd)->
    setTimeout ->
      dfd.resolve("Ping!")
    , 3000
  ).promise()
